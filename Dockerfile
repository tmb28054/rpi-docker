FROM resin/rpi-raspbian
MAINTAINER Topaz Bott <topaz@topazhome.net>

COPY proxy /etc/apt/apt.conf.d/proxy
COPY entrypoint.sh /entrypoint.sh
COPY health-check.sh /health-check.sh

RUN \
  apt-get -y update \
  && apt-get -y upgrade \
  && apt-get install memcached \
  && rm -fv /etc/apt/apt.conf.d/proxy

ENTRYPOINT ["/entrypoint.sh"]

#USER memcache
EXPOSE 11211
