#!/bin/bash

docker run \
  -d \
  --restart=always \
  -p 11211:11211 \
  --health-cmd /health-check.sh \
  --health-interval 20s \
  --health-retries 2 \
  --health-timeout 2s \
  --hostname memcache.topazhome.net \
  tmb28054/rpi-memcached:latest
